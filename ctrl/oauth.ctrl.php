<?php

/**
 * @license Private Porperty
 * @copyright Copyright (c) 2021 by Kronos Analitycs ({@link https://www.kronos-sport.com})
 */
include_once 'base.php';


/**
 * This controller manages actions that can be performed on statut classifications.
 *
 * @url Interface d'auth / inscription
 *
 */
class liboauth_CtrlOauth extends bab_Controller
{
      /**
     * @return string
     */
    protected function getControllerTg()
    {
       $addonName = 'liboauth';
       return  'addon/' . $addonName . '/main';
    }  
    
    
     public function redirectToLogin($form_id)
    {
        $registry = bab_getRegistryInstance();
	    $registry->changeDirectory('/liboauth');
        $fallback = $registry->getValue("loginCallback-".$form_id);
	    
        $action = bab_Widgets()->Action()->fromUrl($fallback);
        
        $action->location();
        die;
    }
}
   