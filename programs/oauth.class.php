<?php
use GuzzleHttp\Cookie\SetCookie;

//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 */
require_once 'base.php';
require_once dirname(__FILE__) . '/functions.php';
require_once dirname(__FILE__) . '/set/form.class.php';


class Func_OAuthServer extends bab_functionality
{
    public $errorMessages = false;


    protected function setInProgress($progress =  true)
    {
        if(!$progress) {
             //$_SESSION["login-isprogress"] = $progress;
             setcookie("login-isprogress", null, time()-3600 ,"/");

           	 setcookie('login-isprogress-data-formId', null, time()-3600 ,"/");
          	 setcookie('login-isprogress-data-scope', null, time()-3600 ,"/");
          	 setcookie('login-isprogress-data-client_id', null, time()-3600 ,"/");
          	 setcookie('login-isprogress-data-response_type', null,time()-3600 ,"/");
          	 setcookie('login-isprogress-data-redirect_uri', null, time()-3600 ,"/");
          	 setcookie('login-isprogress-data-state', null,time()-3600 ,"/");
        } else {
             //$_SESSION["login-isprogress"] = $progress;
             setcookie("login-isprogress", $progress, time()+ (86400 * 30),"/");

           	 setcookie('login-isprogress-data-formId',bab_rp('formId', '') , time()+ (86400 * 30),"/");
          	 setcookie('login-isprogress-data-scope',  bab_rp('scope', ''), time()+ (86400 * 30),"/");
          	 setcookie('login-isprogress-data-client_id',bab_rp('client_id', '') , time()+ (86400 * 30),"/");
          	 setcookie('login-isprogress-data-response_type',bab_rp('response_type', 'code') , time()+ (86400 * 30),"/");
          	 setcookie('login-isprogress-data-redirect_uri',bab_rp('redirect_uri', '') , time()+ (86400 * 30),"/");
          	 setcookie('login-isprogress-data-state', bab_rp('state', ''), time()+ (86400 * 30),"/");
        }
    }

    protected function restoreData()
    {
        /*$_SESSION['LibOAuthServer'] = [//SAVE auth signature datas for use after
            'formId' => $_COOKIE['login-isprogress-data-formId'],
            'scope' => $_COOKIE['login-isprogress-data-scope'],
            'client_id' => $_COOKIE['login-isprogress-data-client_id'],
            'response_type' => $_COOKIE['login-isprogress-data-response_type'],
            'redirect_uri' => $_COOKIE['login-isprogress-data-redirect_uri'],
            'state' => $_COOKIE['login-isprogress-data-state']
        ];*/

        if(isset($_COOKIE['login-isprogress-data-formId'])) {
            $_GET['formId'] = $_COOKIE['login-isprogress-data-formId'];
        }
        if(isset($_COOKIE['login-isprogress-data-scope'])) {
            $_GET['scope'] = $_COOKIE['login-isprogress-data-scope'];
        }
        if(isset($_COOKIE['login-isprogress-data-client_id'])) {
             $_GET['client_id'] = $_COOKIE['login-isprogress-data-client_id'];
        }
        if(isset($_COOKIE['login-isprogress-data-response_type'])) {
             $_GET['response_type'] = $_COOKIE['login-isprogress-data-response_type'];
        }
        if(isset($_COOKIE['login-isprogress-data-redirect_uri'])) {
             $_GET['redirect_uri'] = $_COOKIE['login-isprogress-data-redirect_uri'];
        }
        if(isset($_COOKIE['login-isprogress-data-state'])) {
             $_GET['state'] = $_COOKIE['login-isprogress-data-state'];
        }


    }



    public function isInProgress()
    {
        if (bab_rp('formId', '') && bab_rp('scope', '') && bab_rp('client_id', '') &&  bab_rp('redirect_uri', '') && bab_rp('state', '')) {
            return false;
        }
        if(isset(  $_COOKIE["login-isprogress"]) && $_COOKIE["login-isprogress"]) {
            return true ;
        } else {
            return false;
        }
    }

    protected function storeFormId($formid)
    {
    	setcookie("login-formid", $formid, time()+ (86400 * 30),"/");
    }

    public function getStoredFormId()
    {
        if(isset(  $_COOKIE["login-formid"])) {
            return  $_COOKIE["login-formid"];
        } else {
            return false;
        }
    }

    public function getDescription()
    {
        return 'OAuth server implementation';
    }

    /**
     * Clear all expired entries
     * @return boolean
     */
    public function clearExpires()
    {
        require_once dirname(__FILE__) . '/vendor/autoload.php';
        require_once dirname(__FILE__) . '/OAuthStorage.class.php';
        OAuth2\Autoloader::register();

        $storage = new OAuth2\Storage\OvidentiaOAuthStorage();
        return $storage->clearExpires();
    }

    public function deleteClient($id)
    {
        require_once dirname(__FILE__) . '/vendor/autoload.php';
        require_once dirname(__FILE__) . '/OAuthStorage.class.php';
        OAuth2\Autoloader::register();

        $storage = new OAuth2\Storage\OvidentiaOAuthStorage();
        return $storage->deleteClient($id);
    }


    /**
     * Add a client
     * Scope(s) will be automatically in the list of available scopes
     *
     * @param string $id
     * @param string $pass
     * @param string $redirectUrl
     * @param string $scope
     *
     * @return boolean
     */
    public function addClient($id, $pass, $redirectUrl, $scope = null)
    {
        require_once dirname(__FILE__) . '/vendor/autoload.php';
        require_once dirname(__FILE__) . '/OAuthStorage.class.php';
        OAuth2\Autoloader::register();

        $storage = new OAuth2\Storage\OvidentiaOAuthStorage();
        $storage->addScope($scope);
        return $storage->setClientDetails($id, $pass, $redirectUrl, null, $scope);
    }



    /**
     * Add scope(s) to the available scopes
     *
     * @param string    $scope     space separated scopes
     * @param bool|null $default
     *
     * @return boolean
     */
    public function addScope($scope, $default = null)
    {
        require_once dirname(__FILE__) . '/vendor/autoload.php';
        require_once dirname(__FILE__) . '/OAuthStorage.class.php';
        OAuth2\Autoloader::register();

        $storage = new OAuth2\Storage\OvidentiaOAuthStorage();
        return $storage->addScope($scope, $default);
    }



    /**
     * Delete scope(s) to the available scopes
     *
     * @param string $scope     space separated scopes
     *
     * @return boolean
     */
    public function deleteScope($scope)
    {
        require_once dirname(__FILE__) . '/vendor/autoload.php';
        require_once dirname(__FILE__) . '/OAuthStorage.class.php';
        OAuth2\Autoloader::register();

        $storage = new OAuth2\Storage\OvidentiaOAuthStorage();
        return $storage->deleteScope($scope);
    }


    /**
     * Since version 1.0.0
     *
     * @param integer $lifeTime     lifetime of the generated token in seconde, the value 0 is really 0 seceonde!
     *
     * @throws Exception
     * @return boolean
     */
    public function requestToken($lifeTime = 3600)
    {
        require_once dirname(__FILE__) . '/vendor/autoload.php';
        require_once dirname(__FILE__) . '/OAuthStorage.class.php';
        OAuth2\Autoloader::register();

        $storage = new OAuth2\Storage\OvidentiaOAuthStorage();
        $config = array(
            'access_lifetime' => (int)$lifeTime
        );
        $server = new OAuth2\Server($storage, $config);

        // Add the 'Authorization Code' grant type (this is where the oauth magic happens)
        $server->addGrantType(new OAuth2\GrantType\AuthorizationCode($storage));

        $server->handleTokenRequest(OAuth2\Request::createFromGlobals())->send();
        die;

        return true;
    }


    public function revokeToken($token, $sendError = true, $method = null)
    {
        require_once dirname(__FILE__) . '/vendor/autoload.php';
        require_once dirname(__FILE__) . '/OAuthStorage.class.php';
        OAuth2\Autoloader::register();

        $storage = new OAuth2\Storage\OvidentiaOAuthStorage();
        $server = new OAuth2\Server($storage);

        // Add the 'Authorization Code' grant type (this is where the oauth magic happens)


        $server->addGrantType(new OAuth2\GrantType\AuthorizationCode($storage));

        if ($method == null) {
            $method = $_SERVER['REQUEST_METHOD'];
        }

        $request = new OAuth2\Request(array(), array('token' => $token), array(), array(), array(), array('REQUEST_METHOD' => $method));


        if (!$server->handleRevokeRequest($request)) {
            if ($sendError === true) {
                $server->getResponse()->send();                
                die;
            } else {
                $this->errorMessages = array();
                $error = $server->getResponse()->getParameters();
                if (isset($error['error_description'])) {
                    bab_debug($error['error_description']);
                    $this->errorMessages[] = $error['error_description'];
                }
                return false;
            }
        } else {
            bab_debug("ok");
        }

        return true;
    }


    /**
     * Since version 1.0.0
     * This method has to be call with a POST
     * Get the user id associated to a specific token
     *
     *
     * @throws Exception
     * @return boolean|integer  false on failure or the userid
     */
    public function verifyResourceRequest($token, $scope = null, $sendError = true, $method = null)
    {
        require_once dirname(__FILE__) . '/vendor/autoload.php';
        require_once dirname(__FILE__) . '/OAuthStorage.class.php';
        OAuth2\Autoloader::register();

        $storage = new OAuth2\Storage\OvidentiaOAuthStorage();
        $server = new OAuth2\Server($storage);

        // Add the 'Authorization Code' grant type (this is where the oauth magic happens)
        $server->addGrantType(new OAuth2\GrantType\AuthorizationCode($storage));

        if ($method == null) {
            $method = $_SERVER['REQUEST_METHOD'];
        }

        $request = new OAuth2\Request(array(), array('access_token' => $token), array(), array(), array(), array('REQUEST_METHOD' => $method));
        if (!$server->verifyResourceRequest($request, null, $scope)) {
            if ($sendError === true) {
                $server->getResponse()->send();
                die;
            } else {
                $this->errorMessages = array();
                $error = $server->getResponse()->getParameters();
                if (isset($error['error_description'])) {
                    $this->errorMessages[] = $error['error_description'];
                }
                return false;
            }
        }

        $token = $server->getAccessTokenData($request);

        return $token['user_id'];
    }

    /**
     * Call to add data to register form.
     * @return boolean
     */
    public function addDatasOnRegisterForm($form)
    {
        if (!isset($_SESSION['LibOAuthServer'])) {
            return false;
        }


        $form->setHiddenValue('formId', $_SESSION['LibOAuthServer']['formId']);
        $form->setHiddenValue('scope', $_SESSION['LibOAuthServer']['scope']);
        $form->setHiddenValue('client_id', $_SESSION['LibOAuthServer']['client_id']);
        $form->setHiddenValue('response_type', $_SESSION['LibOAuthServer']['response_type']);
        $form->setHiddenValue('redirect_uri', $_SESSION['LibOAuthServer']['redirect_uri']);
        $form->setHiddenValue('state', $_SESSION['LibOAuthServer']['state']);

        return true;
    }

    protected function saveRegisterPostDatas()
    {
        $_SESSION['LibOAuthServer'] = [//SAVE auth signature datas for use after
            'formId' => bab_rp('formId', ''),
            'scope' => bab_rp('scope', ''),
            'client_id' => bab_rp('client_id', ''),
            'response_type' => bab_rp('response_type', 'code'),
            'redirect_uri' => bab_rp('redirect_uri', ''),
            'state' => bab_rp('state', '')
        ];



        return true;
    }

    protected function facebookLogin()
    {
        /* @var $Facebook Func_Facebook */
        $Facebook = bab_functionality::get('Facebook');

        return $Facebook->getLoginButton();

    }

     protected function googleLogin(){
         /* @var $Google Func_Google */
         $Google = bab_functionality::get('Google');

         return $Google->getLoginButton();

     }

    protected function appleButton(){
        /* @var $Google Func_Apple */
         $Apple = bab_functionality::get('Apple');

         return $Apple->getLoginButton();
    }


    /**
     * Since version 1.0.0
     *
     * @parm string $form_id      Select witch auth form to use
     * @param int $expiration     authorization code expiration time (in seconds) should not be change.
     *
     * @throws Exception
     * @return mixed    could be a widget
     */
    public function process($form_id = null, $scope = '', $expiration = 30)
    {
        if  (!$this->isInProgress()) {
            $this->setInProgress(true);
        }
        $this->restoreData();

        if ($form_id === null && isset($_SESSION['LibOAuthServer'])) {
            $form_id = $_SESSION['LibOAuthServer']['formId'];
            $scope = $_SESSION['LibOAuthServer']['scope'];
        }
        if ($form_id === null){
            throw new Exception('form_id parameter missing from $OAuth->process($formId) call');
        }

        $this->storeFormId($form_id);

        require_once dirname(__FILE__) . '/vendor/autoload.php';
        require_once dirname(__FILE__) . '/OAuthStorage.class.php';
        OAuth2\Autoloader::register();

        $storage = new OAuth2\Storage\OvidentiaOAuthStorage();
        $config = array(
            'auth_code_lifetime' => (int)$expiration
        );
        $server = new OAuth2\Server($storage, $config);

        // Add the 'Authorization Code' grant type (this is where the oauth magic happens)
        $server->addGrantType(new OAuth2\GrantType\AuthorizationCode($storage));

        $request = OAuth2\Request::createFromGlobals();
        $response = new OAuth2\Response();

        // validate the authorize request
        if (!$server->validateAuthorizeRequest($request, $response)) {
            $response->send();
        }

        if (bab_isUserLogged()) {
            $iIdUser = bab_getUserId();
            bab_debug('oAuth otorize after register');
            $server->handleAuthorizeRequest($request, $response, true, $iIdUser);
            $this->setInProgress(false);
            $this->storeFormId(false);
            require_once $GLOBALS['babInstallPath'].'utilit/loginIncl.php';
            bab_logout(false);
            $response->send();
            die;
        }


        $sLogin = bab_pp('nickname', false);
        $sPassword = bab_pp('password', false);
        if ($sLogin !== false && $sPassword !== false) {
            $func = bab_functionality::get('PortalAuthentication/AuthOvidentia');
            /*@var $func Func_PortalAuthentication_AuthOvidentia */

            bab_requireSaveMethod();
            $iIdUser = $func->authenticateUser($sLogin, $sPassword);
            if ($iIdUser) //$func->userCanLogin($iIdUser))
            {
                //NOT NECESSARY!
                //$func->setUserSession($iIdUser, 0);
                //bab_createReversableUserPassword($iIdUser, $sPassword);

                bab_debug('oAuth otorize');
                $server->handleAuthorizeRequest($request, $response, true, $iIdUser);
                require_once $GLOBALS['babInstallPath'].'utilit/loginIncl.php';
                bab_logout(false);
                $response->send();
            } else {
                bab_getBody()->addError('Login or password incorrect.');
            }
        }

       //return $this->getOauthForm($form_id);
    }

    /**

     * @deprecated use sendCallback
     */
    public function getOauthForm($form_id)
    {
         $set = new LibOAuthServer_FormSet();
        $record = $set->get($set->form_id->is($form_id));

        $W = bab_Widgets();
        $page = $W->BabPage()->setLayout($W->VBoxLayout());

        $AI = bab_getAddonInfosInstance('LibOAuthServer');
        $path = new bab_Path($AI->getUploadPath());
        $path->push('logo');
        $path->push($form_id);
        foreach($path as $logo){};

        $skin = $record->skin;
        bab_skin::applyOnCurrentPage($skin, 'ovidentia.css');
        if ($skin === 'LibOAuthServer') {
            $style = $record->css;
            if ($logo) {
                /*@var $T Func_Thumbnailer */
                $T = @bab_functionality::get('Thumbnailer');
                $T->setSourceFile($logo);
                $T->setResizeMode(Func_Thumbnailer::CROP_CENTER, 0.5, 0.5);
                $logo = $T->getThumbnail(149, 27);
                if ($logo) {
                    $style.= '.logo-stack {background:  no-repeat url("'.$logo.'")}';
                }
            }
            $page->addItem($W->Html('<style type="text/css">'.$style.'</style>'));
        }

        $description = null;
        if ($record->description) {
            $description = $W->TextEdit()->setColumns(0)->setValue($record->description)->setDisplayMode()->addClass('liboauthserver-form-description');
        }

        $register = null;
        if ($record->register) {
            $this->saveRegisterPostDatas();

            $register = $W->Link(
                $record->register_label,
                $record->register_url
            )->addClass('oauth-register')->setSizePolicy('widget-align-center');
        }

        $forgotPwd = null;
        if ($record->forgot_pwd) {
            $this->saveRegisterPostDatas();

            $forgotPwd = $W->Link(
                $record->forgot_pwd_label,
                $record->forgot_pwd_url
            )->addClass('oauth-forgot-pwd')->setSizePolicy('widget-align-center');
        }

        $page->addItem(
            $W->VBoxItems(
                $W->Form(null,
                    $W->VBoxItems(
                        $W->Label($record->title)->addClass('liboauthserver-form-title'),
                        $description,
                        $W->LineEdit()/*->setMandatory(true, 'Loggin form has to be fill.')*/->addClass('oauth-nickname form-control')->setPlaceHolder($record->login_label)->setName('nickname')->setSize(20)->setMaxSize(255)->addAttribute('autocorrect', 'off')->addAttribute('autocapitalize', 'none'),
                        $W->LineEdit()/*->setMandatory(true, 'Loggin form has to be fill.')*/->addClass('oauth-password form-control')->obfuscate()->setPlaceHolder($record->password_label)->setName('password')->setSize(20)->setMaxSize(255),
                        $W->SubmitButton()->addClass('oauth-submit')->setLabel($record->button_label)->setSizePolicy('widget-align-center'),
                        $register,
                        $forgotPwd
                    )
                )->setSelfPageHiddenFields()->setHiddenValue('formId', $form_id)
            )->addClass('oauth-details')->setSizePolicy('oauth-body')
        );

        if($record->enableexternal) {
            $page->addItem(
                $W->VBoxItems(
                    $W->FlowItems(
                        $W->Label('')->addClass('kronos-border-login'),
                        $W->Label(kronos_translate("or login with"))->addClass('kronos-register-other'),
                        $W->Label('')->addClass('kronos-border-login')
                    ),
                    $W->FlexItems(
                        $this->facebookLogin(),
                        $this->appleButton(),
                        $this->googleLogin()
                    )->addClass('kronos-register-other-container')->setHorizontalGap(16, 'px')->justifyContent("center")
                )->setHorizontalAlign("center")->setVerticalSpacing(1,"em")
            );
        }

        if ($skin === 'LibOAuthServer') {
            $page->addItem(
                $W->VBoxItems(
                    $W->Label($record->copyright)->setSizePolicy('copyright'),
                    $W->Link($record->pp_text, $record->pp_url)->addAttribute('target', '_blank')->setSizePolicy('privacy')
                )->setFooter()
            );
        }

        return $page;
    }


    public function redirectToLogin($form_id)
    {
        $registry = bab_getRegistryInstance();
	    $registry->changeDirectory('/liboauth');
        $loginCallback = $registry->getValue("loginCallback-".$form_id);

        $action = bab_Widgets()->Action()->fromUrl($loginCallback);

        $action->location();
        die;
    }

    public function registerLoginUrl($url, $form_id)
	{
	    $registry = bab_getRegistryInstance();
	    $registry->changeDirectory('/liboauth');

	    $registry->setKeyValue('loginCallback-'.$form_id, $url);

	    return true;
	}

	public function sendCallback($form_id = null, $scope = '', $expiration = 30)
	{

	    $this->process($form_id, $scope, $expiration);


        $this->redirectToLogin($form_id);


	}

}


/**
 *
 *
 */
class LibOAuthServerException extends Exception {

}