<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 */
require_once 'base.php';


require_once dirname(__FILE__) . '/functions.php';



function LibOAuthServer_upgrade($sVersionBase, $sVersionIni)
{
    $addon = bab_getAddonInfosInstance('LibOAuthServer');

    require_once $GLOBALS['babInstallPath'].'utilit/devtools.php';
    $synchronize = new bab_synchronizeSql();


    require_once dirname(__FILE__) . '/set/accesstoken.class.php';
    require_once dirname(__FILE__) . '/set/client.class.php';
    require_once dirname(__FILE__) . '/set/code.class.php';
    require_once dirname(__FILE__) . '/set/jti.class.php';
    require_once dirname(__FILE__) . '/set/jwt.class.php';
    require_once dirname(__FILE__) . '/set/publickey.class.php';
    require_once dirname(__FILE__) . '/set/refreshtoken.class.php';
    require_once dirname(__FILE__) . '/set/scope.class.php';
    require_once dirname(__FILE__) . '/set/user.class.php';
    require_once dirname(__FILE__) . '/set/form.class.php';

    $mysqlbackend = new ORM_MySqlBackend($GLOBALS['babDB']);
    $sql = 'SET FOREIGN_KEY_CHECKS=0;';

    $sql .= $mysqlbackend->setToSql(new LibOAuthServer_AccessTokenSet()) . "\n";
    $sql .= $mysqlbackend->setToSql(new LibOAuthServer_ClientSet()) . "\n";
    $sql .= $mysqlbackend->setToSql(new LibOAuthServer_CodeSet()) . "\n";
    $sql .= $mysqlbackend->setToSql(new LibOAuthServer_JtiSet()) . "\n";
    $sql .= $mysqlbackend->setToSql(new LibOAuthServer_JwtSet()) . "\n";
    $sql .= $mysqlbackend->setToSql(new LibOAuthServer_PublicKeySet()) . "\n";
    $sql .= $mysqlbackend->setToSql(new LibOAuthServer_RefreshTokenSet()) . "\n";
    $sql .= $mysqlbackend->setToSql(new LibOAuthServer_ScopeSet()) . "\n";
    $sql .= $mysqlbackend->setToSql(new LibOAuthServer_UserSet()) . "\n";
    $sql .= $mysqlbackend->setToSql(new LibOAuthServer_FormSet()) . "\n";

    $synchronize->fromSqlString($sql);


    $registry = bab_getRegistryInstance();
    $registry->changeDirectory("/LibOAuthServer/");
    $exist = $registry->getValue('login_label', null);

    if ($exist !== null) {
        bab_installWindow::message('Import default');
        $set = new LibOAuthServer_FormSet();
        $record = $set->newRecord();
        $record->form_id = 'Default';

        $record->login_label = $exist;
        $record->password_label = $registry->getValue('password_label', null);
        $record->button_label = $registry->getValue('button_label', null);
        $record->title = $registry->getValue('title', null);
        $record->description = $registry->getValue('description', null);
        $record->skin = $registry->getValue('skin', null);
        $record->css = $registry->getValue('css', null);
        $record->copyright = $registry->getValue('copyright', null);
        $record->pp_text = $registry->getValue('pp_text', null);
        $record->pp_url = $registry->getValue('pp_url', null);

        $record->setPrimaryKeyModified(true);
        $record->save();
        $registry->deleteDirectory();

        $AI = bab_getAddonInfosInstance('LibOAuthServer');
        $path = new bab_Path($AI->getUploadPath());
        $path->push('logo');

        $formPath = new bab_Path($AI->getUploadPath());
        $formPath->push('logo');
        $formPath->push($record->form_id);
        $formPath->createDir();
        foreach ($path as $file) {
            /* @var bab_Path $file */
            if($file->isFile()) {
                $formPath->push($file->getBasename());
                rename($file->tostring(), $formPath->tostring());
                break;
            }
        }
    }


//     require_once dirname(__FILE__) . '/vendor/autoload.php';
//     require_once dirname(__FILE__) . '/OAuthStorage.class.php';
//     OAuth2\Autoloader::register();

//     $storage = new OAuth2\Storage\OvidentiaOAuthStorage();
//     $server = new OAuth2\Server($storage);

//     // Add the "Client Credentials" grant type
//     $server->addGrantType(new OAuth2\GrantType\ClientCredentials($storage));

//     // Add the "Authorization Code" grant type
//     $server->addGrantType(new OAuth2\GrantType\AuthorizationCode($storage));

    $addon->registerFunctionality('Func_OAuthServer', 'oauth.class.php');

    return true;
}


function LibOAuthServer_onDeleteAddon()
{
    $addon = bab_getAddonInfosInstance('LibOAuthServer');
    $addon->unregisterFunctionality('Func_OAuthServer');

    return true;
}
