<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2008 by CANTICO ({@link http://www.cantico.fr})
 */
include_once 'base.php';

if (!bab_isUserAdministrator()) {
	die;
}

require_once dirname(__FILE__) . '/functions.php';
require_once dirname(__FILE__) . '/set/form.class.php';
bab_Widgets()->includePhpClass('widget_TableView');

class LibOAuthServer_FormTableView extends widget_TableModelView
{

    /**
     * {@inheritDoc}
     * @see widget_TableModelView::addDefaultColumns()
     */
    public function addDefaultColumns(ORM_RecordSet $set)
    {
        $this->addColumn(widget_TableModelViewColumn($set->form_id, 'Form id'));
    }


    /**
     * {@inheritDoc}
     * @see widget_TableModelView::computeCellContent($record, $fieldPath)
     */
    protected function computeCellContent(ORM_Record $record, $fieldPath)
    {
        $W = bab_Widgets();

        switch ($fieldPath) {
            case 'form_id':
                return $W->Link(
                    $record->form_id,
                    '?tg=addon/LibOAuthServer/configuration&idx=config&id='.$record->form_id
                );
        }

        return parent::computeCellContent($record, $fieldPath);
    }
}

/**
 * Displays the addon configuration page.
 *
 * @param string $message
 */
function LibOAuthServer_list()
{
    $W = bab_Widgets();

    $set = new LibOAuthServer_FormSet();
    $forms = $set->select();
    $forms->orderAsc($set->form_id);

    $tableView = new LibOAuthServer_FormTableView();
    $tableView->addDefaultColumns($set);
    $tableView->setDataSource($forms);

    $page = $W->BabPage();
    $page->addItem(
        $W->VBoxItems(
            $tableView,
            $W->Link('Add a new form', '?tg=addon/LibOAuthServer/configuration&idx=config')
        )->setVerticalSpacing(10, 'px')
    );

    $page->displayHtml();
}


/**
 * Displays the addon configuration page.
 *
 * @param string $message
 */
function LibOAuthServer_configuration($id = '')
{
	$W = bab_Widgets();

	$list = bab_skin::getList();
	$options = array('LibOAuthServer' => LibOAuthServer_translate('default OAuth skin'));
	foreach($list as $skin) {
	    $options[$skin->getName()] = $skin->getName();
	}

	$AI = bab_getAddonInfosInstance('LibOAuthServer');
	$path = new bab_Path($AI->getUploadPath());
	$path->push('tmplogo');
	$path->createDir();

	$page = $W->BabPage();
	$page->addItem(
	    $form = $W->Form()->setSelfPageHiddenFields()->setHiddenValue('idx', 'save')->setName('LibOAuthServer')->setLayout(
	        $W->VBoxItems(
	            $W->LabelledWidget(
	                LibOAuthServer_translate('Form id'),
	                $W->LineEdit()->addClass('widget-100pc')->setMandatory(true, LibOAuthServer_translate('The Form id is mandatory.')),
	                'form_id'
                ),
    		    $W->LabelledWidget(
    		        LibOAuthServer_translate('Login label'),
    		        $W->LineEdit()->addClass('widget-100pc')->setMandatory(true, LibOAuthServer_translate('The login label is mandatory.')),
    		        'login_label'
		        ),
    		    $W->LabelledWidget(
    		        LibOAuthServer_translate('Password label'),
    		        $W->LineEdit()->addClass('widget-100pc')->setMandatory(true, LibOAuthServer_translate('The password label is mandatory.')),
    		        'password_label'
		        ),
    		    $W->LabelledWidget(
    		        LibOAuthServer_translate('Button label'),
    		        $W->LineEdit()->addClass('widget-100pc')->setMandatory(true, LibOAuthServer_translate('The button label is mandatory.')),
    		        'button_label'
		        ),
    		    $W->LabelledWidget(
    		        LibOAuthServer_translate('Title'),
    		        $W->LineEdit()->addClass('widget-100pc'),
    		        'title'
		        ),
    		    $W->LabelledWidget(
    		        LibOAuthServer_translate('Description text (appear abbove the form can be empty)'),
    		        $W->TextEdit()->addClass('widget-100pc'),
    		        'description'
		        ),
	            //EXTERNAL
	            $W->LabelledWidget(
	                LibOAuthServer_translate('Enable external buttons'),
	                $W->CheckBox()->setUncheckedValue(0)->setCheckedValue(1),
	                'enableexternal',
	                LibOAuthServer_translate('')
                ),
	            
	            //REGISTER
	            $W->LabelledWidget(
	                LibOAuthServer_translate('Show register link'),
	                $W->CheckBox()->setUncheckedValue(0)->setCheckedValue(1),
	                'register',
	                LibOAuthServer_translate('For implementation you have to call $OAuth->addDatasOnRegisterForm(form) passing register form and call $OAuth->process() after login the user.')
                ),
	            $W->LabelledWidget(
	                LibOAuthServer_translate('Register url'),
	                $W->UrlLineEdit()->setSchemeCheck(false)->addClass('widget-100pc'),
	                'register_url'
                ),
	            $W->LabelledWidget(
	                LibOAuthServer_translate('Register label'),
	                $W->LineEdit()->addClass('widget-100pc'),
	                'register_label'
                ),
	            //FORGOT PASSWORD
	            $W->LabelledWidget(
	                LibOAuthServer_translate('Show forgot password'),
	                $W->CheckBox()->setUncheckedValue(0)->setCheckedValue(1),
	                'forgot_pwd',
	                LibOAuthServer_translate('For implementation you have to call $OAuth->addDatasOnRegisterForm(form) passing lost password form and call $OAuth->process() after user logged in.')
                ),
	            $W->LabelledWidget(
	                LibOAuthServer_translate('Forgot password url'),
	                $W->UrlLineEdit()->setSchemeCheck(false)->addClass('widget-100pc'),
	                'forgot_pwd_url'
                ),
	            $W->LabelledWidget(
	                LibOAuthServer_translate('Forgot password label'),
	                $W->LineEdit()->addClass('widget-100pc'),
	                'forgot_pwd_label'
                ),
	            //SKIN
    		    $W->LabelledWidget(
    		        LibOAuthServer_translate('Skin used'),
    		        $W->Select()->setOptions($options)->addClass('widget-100pc')->setMandatory(true, LibOAuthServer_translate('The skin is mandatory.')),
    		        'skin'
		        ),
    		    $W->Section(
    		        LibOAuthServer_translate('Default skin configuration'),
    		        $W->VBoxItems(
    		            $image = $W->ImagePicker()->oneFileMode()->setButtonLabel(LibOAuthServer_translate('Logo'))->setFolder($path),
    		            $W->LabelledWidget(
    		                LibOAuthServer_translate('Personalisation (CSS)'),
    		                $W->TextEdit()->addClass('widget-100pc'),
    		                'css'
		                ),
    		            $W->LabelledWidget(
    		                LibOAuthServer_translate('Copyright text'),
    		                $W->LineEdit()->addClass('widget-100pc')->setValue('Copyright © 2020'),
    		                'copyright'
		                ),
    		            $W->LabelledWidget(
    		                LibOAuthServer_translate('Privacy policy text'),
    		                $W->LineEdit()->addClass('widget-100pc'),
    		                'pp_text'
		                ),
    		            $W->LabelledWidget(
    		                LibOAuthServer_translate('Privacy policy url'),
    		                $W->UrlLineEdit()->setSchemeCheck(false)->addClass('widget-100pc'),
    		                'pp_url'
		                )
		            )->setVerticalSpacing(1, 'em')
		        )->addParentAttribute('style', 'border: 1px solid #bbb; padding: 10px; border-radius: 10px; margin-top: 1em; background-color: #eee;'),
    		    $W->SubmitButton()->setLabel('Save')->validate()
    		)->setVerticalSpacing(1, 'em')
        )
	);

	if ($id) {
	    $set = new LibOAuthServer_FormSet();
	    $record = $set->get($set->form_id->is($id));
	    if ($record) {
	        $form->setValues($record->getValues(), array('LibOAuthServer'));
	        $form->addItem($W->Hidden(null, 'old_id', $id));

	        $path = new bab_Path($AI->getUploadPath());
	        $path->push('logo');
	        $path->push($id);
	        $image->setFolder($path);
	    }
	}

	$page->displayHtml();
}




/**
 * Saves the addon configuration.
 *
 * @param array $config
 * @return boolean
 */
function LibOAuthServer_saveConfiguration()
{
	global $babBody;

	$LibOAuthServer = bab_rp('LibOAuthServer', array());

	if($LibOAuthServer && !empty($LibOAuthServer)) {
	    $set = new LibOAuthServer_FormSet();
	    $AI = bab_getAddonInfosInstance('LibOAuthServer');

	    if (isset($LibOAuthServer['old_id']) && $LibOAuthServer['old_id']) {
	        $set->delete($set->form_id->is($LibOAuthServer['old_id']));


	        $path = new bab_Path($AI->getUploadPath());
	        $path->push('logo');
	        $path->push($LibOAuthServer['old_id']);

	        $formPath = new bab_Path($AI->getUploadPath());
	        $formPath->push('logo');
	        $formPath->push($LibOAuthServer['form_id']);

	        rename($path->tostring(), $formPath->tostring());
	    }

	    $record = $set->get($set->form_id->is($LibOAuthServer['form_id']));
	    if (!$record) {
	        $record = $set->newRecord();
	        $record->form_id = $LibOAuthServer['form_id'];
	    } else {
	        $babBody->addNextPageMessage(LibOAuthServer_translate('From id already exist.'));
	        header("location: ?tg=addon/LibOAuthServer/configuration&save=-1");
	    }

	    $record->login_label = $LibOAuthServer['login_label'];
	    $record->password_label = $LibOAuthServer['password_label'];
	    $record->button_label = $LibOAuthServer['button_label'];
	    $record->title = $LibOAuthServer['title'];
	    $record->description = $LibOAuthServer['description'];

	    $record->enableexternal = $LibOAuthServer['enableexternal'];
	    

	    $record->register = $LibOAuthServer['register'];
	    $record->register_url = $LibOAuthServer['register_url'];
	    $record->register_label = $LibOAuthServer['register_label'];

	    $record->forgot_pwd = $LibOAuthServer['forgot_pwd'];
	    $record->forgot_pwd_url = $LibOAuthServer['forgot_pwd_url'];
	    $record->forgot_pwd_label = $LibOAuthServer['forgot_pwd_label'];

	    $record->skin = $LibOAuthServer['skin'];
	    $record->css = $LibOAuthServer['css'];
	    $record->copyright = $LibOAuthServer['copyright'];
	    $record->pp_text = $LibOAuthServer['pp_text'];
	    $record->pp_url = $LibOAuthServer['pp_url'];

	    $record->setPrimaryKeyModified(true);
	    $record->save();

	    if (!isset($LibOAuthServer['old_id']) || !$LibOAuthServer['old_id']) {
    	    $AI = bab_getAddonInfosInstance('LibOAuthServer');
    	    $path = new bab_Path($AI->getUploadPath());
    	    $path->push('tmplogo');

    	    $formPath = new bab_Path($AI->getUploadPath());
    	    $formPath->push('logo');
    	    $formPath->push($record->form_id);
    	    $formPath->createDir();
    	    foreach ($path as $file) {
    	        /* @var bab_Path $file */
    	        if($file->isFile()) {
    	            $formPath->push($file->getBasename());
    	            rename($file->tostring(), $formPath->tostring());
    	            break;
    	        }
    	    }
	    }
    	$babBody->addNextPageMessage(LibOAuthServer_translate('Configuration saved.'));
	} else {
	    $babBody->addNextPageMessage(LibOAuthServer_translate('Error, configuration missing.'));
	}
	header("location: ?tg=addon/LibOAuthServer/configuration");
}

$idx = bab_rp('idx');

switch($idx) {

	case 'save':
	    LibOAuthServer_saveConfiguration();
		break;

	case 'config':
		LibOAuthServer_configuration(bab_rp('id', ''));
		break;

	default:
	case 'list':
	    LibOAuthServer_list();
	    break;
}
