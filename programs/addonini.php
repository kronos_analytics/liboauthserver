; <?php/*

[general]
name							="LibOAuthServer"
addon_type                      ="LIBRARY"
version							="4.0.1"
encoding						="UTF-8"
description						="Library to use ovidentia as a OAuth server"
description.fr					="Bibliothèque pour l'utilisation d'Ovidentia comme un server OAuth"
delete							=1
ov_version						="8.5.0"
php_version						="5.4.0"
addon_access_control			=0
author							="Kronos Analytics"
icon							="icon.png"
mysql_character_set_database	="latin1,utf8"
configuration_page              ="configuration"

;*/?>