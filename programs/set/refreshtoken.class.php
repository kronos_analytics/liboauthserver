<?php
/**
 * @license Private Porperty
 * @copyright Copyright (c) 2019 by Kronos Analitycs ({@link https://www.kronos-sport.com})
 */




/**
 *
 * @method LibOAuthServer_RefreshToken  get
 */
class LibOAuthServer_RefreshTokenSet extends ORM_RecordSet
{
	public function __construct()
	{
	    parent::__construct();

		$this->addFields(
		    ORM_StringField('refresh_token', 40),
		    ORM_StringField('client_id', 80),
		    ORM_StringField('user_id', 80),
		    ORM_TimestampField('expires'),
		    ORM_StringField('scope', 4000)
	    );

		$this->setPrimaryKey('refresh_token');
	}
}






/**
 *
 */
class LibOAuthServer_RefreshToken extends ORM_Record
{

}