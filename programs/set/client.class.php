<?php
/**
 * @license Private Porperty
 * @copyright Copyright (c) 2019 by Kronos Analitycs ({@link https://www.kronos-sport.com})
 */




/**
 *
 * @method LibOAuthServer_ClientSet  get
 * @method LibOAuthServer_ClientSet  newRecord
 */
class LibOAuthServer_ClientSet extends ORM_RecordSet
{

	public function __construct()
	{
	    parent::__construct();

		$this->addFields(
		    ORM_StringField('client_id', 80),
		    ORM_StringField('client_secret', 80),
		    ORM_UrlField('redirect_uri', 2000),
		    ORM_StringField('grant_types', 80),
		    ORM_StringField('scope', 4000),
		    ORM_StringField('user_id', 80)
	    );

		$this->setPrimaryKey('client_id');
	}
}






/**
 *
 */
class LibOAuthServer_Client extends ORM_Record
{

}