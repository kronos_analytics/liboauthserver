<?php
/**
 * @license Private Porperty
 * @copyright Copyright (c) 2019 by Kronos Analitycs ({@link https://www.kronos-sport.com})
 */




/**
 *
 * @method LibOAuthServer_PublicKey  get
 */
class LibOAuthServer_PublicKeySet extends ORM_RecordSet
{
	public function __construct()
	{
	    parent::__construct();

	    $this->setPrimaryKey('id');

		$this->addFields(
		    ORM_StringField('client_id', 80)->setNullAllowed(),
		    ORM_StringField('public_key', 2000),
		    ORM_StringField('private_key', 2000),
		    ORM_StringField('encryption_algorithm', 100)//DEFAULT RS256
	    );
	}
}






/**
 *
 */
class LibOAuthServer_PublicKey extends ORM_Record
{

}