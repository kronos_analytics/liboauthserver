<?php
/**
 * @license Private Porperty
 * @copyright Copyright (c) 2019 by Kronos Analitycs ({@link https://www.kronos-sport.com})
 */




/**
 *
 * @method LibOAuthServer_User  get
 */
class LibOAuthServer_UserSet extends ORM_RecordSet
{
	public function __construct()
	{
	    parent::__construct();

		$this->addFields(
		    ORM_UserField('user_id', 80),
		    ORM_StringField('scope', 4000)
	    );

		$this->setPrimaryKey('user_id');
	}

	public function getFromUsername($username)
	{
	    $user = bab_getUserByNickname($username);

	    if (!$user) {
	        return false;
	    }
	    $userRecord = $this->get($this->user_id->is($user['id']));

	    if (!$userRecord) {
	        return false;
	    }

	    return array(
	        'username' => $username,
	        'password' => $user['password'],
	        'password_hash_function' => $user['password_hash_function'],
	        'first_name' => $user['firstname'],
	        'last_name' => $user['lastname'],
	        'email' => $user['email'],
	        'email_verified' => true,
	        'scope' => $userRecord->scope
	    );
	}
}






/**
 *
 */
class LibOAuthServer_User extends ORM_Record
{

}