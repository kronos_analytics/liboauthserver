<?php
/**
 * @license Private Porperty
 * @copyright Copyright (c) 2019 by Kronos Analitycs ({@link https://www.kronos-sport.com})
 */



/**
 *
 * @method LibOAuthServer_FormSet  get
 * @method LibOAuthServer_FormSet  newRecord
 */
class LibOAuthServer_FormSet extends ORM_RecordSet
{

	public function __construct()
	{
	    parent::__construct();

		$this->addFields(
		    ORM_StringField('form_id'),
		    ORM_StringField('login_label'),
		    ORM_StringField('password_label'),
		    ORM_StringField('button_label'),
		    ORM_StringField('title'),
		    ORM_TextField('description'),
		    ORM_StringField('skin'),
		    ORM_TextField('css'),
		    ORM_StringField('copyright'),
		    ORM_StringField('pp_text'),
		    ORM_UrlField('pp_url'),
		    ORM_BoolField('register'),
		    ORM_BoolField('enableexternal'), 
		    ORM_UrlField('register_url'),
		    ORM_StringField('register_label'),
		    ORM_BoolField('forgot_pwd'),
		    ORM_UrlField('forgot_pwd_url'),
		    ORM_StringField('forgot_pwd_label')
	    );

		$this->setPrimaryKey('form_id');
	}
}






/**
 *
 */
class LibOAuthServer_Form extends ORM_Record
{

}