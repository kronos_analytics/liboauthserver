<?php
/**
 * @license Private Porperty
 * @copyright Copyright (c) 2019 by Kronos Analitycs ({@link https://www.kronos-sport.com})
 */




/**
 *
 * @method LibOAuthServer_Jwt  get
 */
class LibOAuthServer_JwtSet extends ORM_RecordSet
{
	public function __construct()
	{
	    parent::__construct();

	    $this->setPrimaryKey('id');

		$this->addFields(
		    ORM_StringField('client_id', 80),
		    ORM_StringField('subject', 80),
		    ORM_StringField('public_key', 2000)
	    );
	}
}






/**
 *
 */
class LibOAuthServer_Jwt extends ORM_Record
{

}