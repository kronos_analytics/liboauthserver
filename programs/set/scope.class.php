<?php
/**
 * @license Private Porperty
 * @copyright Copyright (c) 2019 by Kronos Analitycs ({@link https://www.kronos-sport.com})
 */




/**
 *
 * @method LibOAuthServer_Scope  get
 */
class LibOAuthServer_ScopeSet extends ORM_RecordSet
{
	public function __construct()
	{
	    parent::__construct();

		$this->addFields(
		    ORM_StringField('scope', 80),
		    ORM_BoolField('is_default')
	    );

		$this->setPrimaryKey('scope');
	}
}






/**
 *
 */
class LibOAuthServer_Scope extends ORM_Record
{

}