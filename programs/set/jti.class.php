<?php
/**
 * @license Private Porperty
 * @copyright Copyright (c) 2019 by Kronos Analitycs ({@link https://www.kronos-sport.com})
 */




/**
 *
 * @method LibOAuthServer_Jti  get
 */
class LibOAuthServer_JtiSet extends ORM_RecordSet
{
	public function __construct()
	{
	    parent::__construct();

	    $this->setPrimaryKey('id');

		$this->addFields(
		    ORM_StringField('issuer', 80),
		    ORM_StringField('subject', 80),
		    ORM_StringField('audiance', 80),
		    ORM_TimestampField('expires'),
		    ORM_StringField('jti', 2000)
	    );
	}
}






/**
 *
 */
class LibOAuthServer_Jti extends ORM_Record
{

}