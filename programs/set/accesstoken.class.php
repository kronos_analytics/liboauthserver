<?php
/**
 * @license Private Porperty
 * @copyright Copyright (c) 2019 by Kronos Analitycs ({@link https://www.kronos-sport.com})
 */




/**
 *
 * @method LibOAuthServer_AccessToken  get
 */
class LibOAuthServer_AccessTokenSet extends ORM_RecordSet
{


	public function __construct()
	{
	    parent::__construct();

		$this->addFields(
		    ORM_StringField('access_token', 40),
		    ORM_StringField('client_id', 80),
		    ORM_StringField('user_id', 80),
		    ORM_TimestampField('expires'),
		    ORM_StringField('scope', 4000)
	    );

		$this->setPrimaryKey('access_token');
	}
}






/**
 *
 */
class LibOAuthServer_AccessToken extends ORM_Record
{

}