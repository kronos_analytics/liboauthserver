<?php
/**
 * @license Private Porperty
 * @copyright Copyright (c) 2019 by Kronos Analitycs ({@link https://www.kronos-sport.com})
 */




/**
 *
 * @method LibOAuthServer_Code  get
 */
class LibOAuthServer_CodeSet extends ORM_RecordSet
{

	public function __construct()
	{
	    parent::__construct();

		$this->addFields(
		    ORM_StringField('authorization_code', 40),
		    ORM_StringField('client_id', 80),
		    ORM_StringField('user_id', 80),
		    ORM_UrlField('redirect_uri', 2000),
		    ORM_TimestampField('expires'),
		    ORM_StringField('scope', 4000),
		    ORM_StringField('id_token', 1000)
	    );

		$this->setPrimaryKey('authorization_code');
	}
}






/**
 *
 */
class LibOAuthServer_Code extends ORM_Record
{

}