<?php

/**
 * @license Private Porperty
 * @copyright Copyright (c) 2021 by Kronos Analitycs ({@link https://www.kronos-sport.com})
 */
include_once 'base.php';
require_once dirname(__FILE__) . '/functions.php';

require_once $GLOBALS['babInstallPath'].'utilit/controller.class.php';

class liboauth_Controller extends bab_Controller
{
    public $controllerTg;
    
     /**
     * @return libgoogle_CtrlGoogle
     */
    public function Oauth($proxy = true)
    {

        require_once dirname(__FILE__) . '/ctrl/oauth.ctrl.php';
        return $this->ControllerProxy('liboauth_CtrlOauth', $proxy);
    }
    
    
    /**
     * @return string
     */
    protected function getControllerTg()
    {
       $addonName = 'liboauth';
       return  'addon/' . $addonName . '/main';
    } 
}