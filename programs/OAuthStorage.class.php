<?php
/**
 * @license Private Porperty
 * @copyright Copyright (c) 2019 by Kronos Analitycs ({@link https://www.kronos-sport.com})
 */

namespace OAuth2\Storage;

require_once dirname(__FILE__) . '/vendor/autoload.php';

require_once dirname(__FILE__) . '/set/accesstoken.class.php';
require_once dirname(__FILE__) . '/set/client.class.php';
require_once dirname(__FILE__) . '/set/code.class.php';
require_once dirname(__FILE__) . '/set/jti.class.php';
require_once dirname(__FILE__) . '/set/jwt.class.php';
require_once dirname(__FILE__) . '/set/publickey.class.php';
require_once dirname(__FILE__) . '/set/refreshtoken.class.php';
require_once dirname(__FILE__) . '/set/scope.class.php';
require_once dirname(__FILE__) . '/set/user.class.php';



use OAuth2\OpenID\Storage\UserClaimsInterface;
use OAuth2\OpenID\Storage\AuthorizationCodeInterface as OpenIDAuthorizationCodeInterface;

class OvidentiaOAuthStorage implements
AuthorizationCodeInterface,
AccessTokenInterface,
ClientCredentialsInterface,
UserCredentialsInterface,
RefreshTokenInterface,
JwtBearerInterface,
ScopeInterface,
PublicKeyInterface,
UserClaimsInterface,
OpenIDAuthorizationCodeInterface
{

    /**
     * @var array
     */
    protected $config;

    /**
     * @param mixed $connection
     * @param array $config
     *
     */
    public function __construct()
    {
        $AccessTokenSet = new \LibOAuthServer_AccessTokenSet();
        $ClientSet = new \LibOAuthServer_ClientSet();
        $CodeSet = new \LibOAuthServer_CodeSet();
        $JtiSet = new \LibOAuthServer_JtiSet();
        $JwtSet = new \LibOAuthServer_JwtSet();
        $PublicKeySet = new \LibOAuthServer_PublicKeySet();
        $RefreshTokenSet = new \LibOAuthServer_RefreshTokenSet();
        $ScopeSet = new \LibOAuthServer_ScopeSet();
        $UserSet = new \LibOAuthServer_UserSet();

        $this->config = array(
            'client_table' => $ClientSet->getTableName(),
            'access_token_table' => $AccessTokenSet->getTableName(),
            'refresh_token_table' => $RefreshTokenSet->getTableName(),
            'code_table' => $CodeSet->getTableName(),
            'user_table' => $UserSet->getTableName(),
            'jwt_table'  => $JwtSet->getTableName(),
            'jti_table'  => $JtiSet->getTableName(),
            'scope_table'  => $ScopeSet->getTableName(),
            'public_key_table'  => $PublicKeySet->getTableName()
        );
    }

    /**
     * @param string $client_id
     * @param null|string $client_secret
     * @return bool
     */
    public function checkClientCredentials($client_id, $client_secret = null)
    {
        $clientSet = new \LibOAuthServer_ClientSet();
        $client = $clientSet->get($clientSet->client_id->is($client_id));

        // make this extensible
        return $client && $client->client_secret == $client_secret;
    }

    /**
     * @param string $client_id
     * @return bool
     */
    public function isPublicClient($client_id)
    {
        $clientSet = new \LibOAuthServer_ClientSet();
        $client = $clientSet->get($clientSet->client_id->is($client_id));

        if(!$client) {
            return false;
        }

        return empty($client->client_secret);
    }

    /**
     * @param string $client_id
     * @return array|mixed
     */
    public function getClientDetails($client_id)
    {
        $clientSet = new \LibOAuthServer_ClientSet();
        $client = $clientSet->get($clientSet->client_id->is($client_id));

        if (!$client) {
            return false;
        }

        return $client->getValues();
    }

    /**
     * @param string $client_id
     * @return bool
     */
    public function deleteClient($client_id)
    {
        $clientSet = new \LibOAuthServer_ClientSet();
        $clientSet->delete($clientSet->client_id->is($client_id));
        return true;
    }

    /**
     * @param string $client_id
     * @param null|string $client_secret
     * @param null|string $redirect_uri
     * @param null|array  $grant_types
     * @param null|string $scope
     * @param null|string $user_id
     * @return bool
     */
    public function setClientDetails($client_id, $client_secret = null, $redirect_uri = null, $grant_types = null, $scope = null, $user_id = null)
    {
        $clientSet = new \LibOAuthServer_ClientSet();
        $client = $clientSet->get($clientSet->client_id->is($client_id));

        if (!$client) {
            $client = $clientSet->newRecord();
            $client->client_id = $client_id;
            $client->setPrimaryKeyModified(true);//BUG WITH string primarykey
        }

        if ($grant_types === null) {
            $grant_types = array();
        }

        $client->client_secret = $client_secret;
        $client->redirect_uri = $redirect_uri;
        $client->grant_types = implode(' ', $grant_types);
        $client->scope = $scope;
        $client->user = $user_id;

        return $client->save();
    }

    /**
     * @param $client_id
     * @param $grant_type
     * @return bool
     */
    public function checkRestrictedGrantType($client_id, $grant_type)
    {
        $details = $this->getClientDetails($client_id);

        if (isset($details['grant_types']) && $details['grant_types']) {
            $grant_types = explode(' ', $details['grant_types']);

            return in_array($grant_type, (array) $grant_types);
        }

        // if grant_types are not defined, then none are restricted
        return true;
    }

    /**
     * @param string $access_token
     * @return array|bool|mixed|null
     */
    public function getAccessToken($access_token)
    {
        $accessTokenSet = new \LibOAuthServer_AccessTokenSet();
        $accessToken = $accessTokenSet->get($accessTokenSet->access_token->is($access_token));

        if ($accessToken) {
            // convert date string back to timestamp
            $accessToken->expires = strtotime($accessToken->expires);
        } else {
            return false;
        }

        return $accessToken->getValues();
    }

    /**
     * @param string $access_token
     * @param mixed  $client_id
     * @param mixed  $user_id
     * @param int    $expires
     * @param string $scope
     * @return bool
     */
    public function setAccessToken($access_token, $client_id, $user_id, $expires, $scope = null)
    {
        $accessTokenSet = new \LibOAuthServer_AccessTokenSet();
        $accessToken = $accessTokenSet->get($accessTokenSet->access_token->is($access_token));

        if (!$accessToken) {
            $accessToken = $accessTokenSet->newRecord();
            $accessToken->access_token = $access_token;
            $accessToken->setPrimaryKeyModified(true);//BUG WITH string primarykey
        }

        if ($scope === null) {
            $scope = array();
        }

        $accessToken->client_id = $client_id;
        $accessToken->user_id = $user_id;
        $accessToken->expires = date('Y-m-d H:i:s', $expires);;
        $accessToken->scope = $scope;

        return $accessToken->save();
    }

    /**
     * @param $access_token
     * @return bool
     */
    public function unsetAccessToken($access_token)
    {
        $accessTokenSet = new \LibOAuthServer_AccessTokenSet();

        return $accessTokenSet->delete($accessTokenSet->access_token->is($access_token));
    }

    /* OAuth2\Storage\AuthorizationCodeInterface */
    /**
     * @param string $code
     * @return mixed
     */
    public function getAuthorizationCode($code)
    {
        $codeSet = new \LibOAuthServer_CodeSet();
        $code = $codeSet->get($codeSet->authorization_code->is($code));

        if (!$code) {
            return false;
        } else {
            $code->expires = strtotime($code->expires);
        }

        return $code->getValues();
    }

    /**
     * @param string $code
     * @param mixed  $client_id
     * @param mixed  $user_id
     * @param string $redirect_uri
     * @param int    $expires
     * @param string $scope
     * @param string $id_token
     * @return bool|mixed
     */
    public function setAuthorizationCode($code, $client_id, $user_id, $redirect_uri, $expires, $scope = null, $id_token = null)
    {
        if (func_num_args() > 6) {
            // we are calling with an id token
            return call_user_func_array(array($this, 'setAuthorizationCodeWithIdToken'), func_get_args());
        }

        // convert expires to datestring
        $expires = date('Y-m-d H:i:s', $expires);

        $codeSet = new \LibOAuthServer_CodeSet();
        $codeRecord = $codeSet->get($codeSet->authorization_code->is($code));

        if (!$codeRecord) {
            $codeRecord = $codeSet->newRecord();
            $codeRecord->authorization_code = $code;
            $codeRecord->setPrimaryKeyModified(true);//BUG WITH string primarykey
        }

        $codeRecord->client_id = $client_id;
        $codeRecord->user_id = $user_id;
        $codeRecord->redirect_uri = $redirect_uri;
        $codeRecord->expires = $expires;
        $codeRecord->scope = $scope;
        $codeRecord->id_token = $id_token;

        return $codeRecord->save();
    }

    /**
     * @param string $code
     * @param mixed  $client_id
     * @param mixed  $user_id
     * @param string $redirect_uri
     * @param string $expires
     * @param string $scope
     * @param string $id_token
     * @return bool
     */
    private function setAuthorizationCodeWithIdToken($code, $client_id, $user_id, $redirect_uri, $expires, $scope = null, $id_token = null)
    {
        // convert expires to datestring
        $expires = date('Y-m-d H:i:s', $expires);

        $codeSet = new \LibOAuthServer_CodeSet();
        $codeRecord = $codeSet->get($codeSet->authorization_code->is($code));

        if (!$codeRecord) {
            $codeRecord = $codeSet->newRecord();
            $codeRecord->authorization_code = $code;
            $codeRecord->setPrimaryKeyModified(true);//BUG WITH string primarykey
        }

        $codeRecord->client_id = $client_id;
        $codeRecord->user_id = $user_id;
        $codeRecord->redirect_uri = $redirect_uri;
        $codeRecord->expires = $expires;
        $codeRecord->id_token = $id_token;
        $codeRecord->scope = $scope;

        return $codeRecord->save();
    }

    /**
     * @param string $code
     * @return bool
     */
    public function expireAuthorizationCode($code)
    {
        $codeSet = new \LibOAuthServer_CodeSet();

        return $codeSet->delete($codeSet->authorization_code->is($code));
    }

    /**
     * @param string $username
     * @param string $password
     * @return bool
     */
    public function checkUserCredentials($username, $password)
    {
        if ($user = $this->getUser($username)) {
            return $this->checkPassword($user, $password);
        }

        return false;
    }

    /**
     * @param string $username
     * @return array|bool
     */
    public function getUserDetails($username)
    {
        return $this->getUser($username);
    }

    /**
     * @param mixed  $user_id
     * @param string $claims
     * @return array|bool
     */
    public function getUserClaims($user_id, $claims)
    {
        if (!$userDetails = $this->getUserDetails($user_id)) {
            return false;
        }

        $claims = explode(' ', trim($claims));
        $userClaims = array();

        // for each requested claim, if the user has the claim, set it in the response
        $validClaims = explode(' ', self::VALID_CLAIMS);
        foreach ($validClaims as $validClaim) {
            if (in_array($validClaim, $claims)) {
                if ($validClaim == 'address') {
                    // address is an object with subfields
                    $userClaims['address'] = $this->getUserClaim($validClaim, $userDetails['address'] ?: $userDetails);
                } else {
                    $userClaims = array_merge($userClaims, $this->getUserClaim($validClaim, $userDetails));
                }
            }
        }

        return $userClaims;
    }

    /**
     * @param string $claim
     * @param array  $userDetails
     * @return array
     */
    protected function getUserClaim($claim, $userDetails)
    {
        $userClaims = array();
        $claimValuesString = constant(sprintf('self::%s_CLAIM_VALUES', strtoupper($claim)));
        $claimValues = explode(' ', $claimValuesString);

        foreach ($claimValues as $value) {
            $userClaims[$value] = isset($userDetails[$value]) ? $userDetails[$value] : null;
        }

        return $userClaims;
    }

    /**
     * @param string $refresh_token
     * @return bool|mixed
     */
    public function getRefreshToken($refresh_token)
    {
        $refreshTokenSet = new \LibOAuthServer_RefreshTokenSet();
        $refreshToken = $refreshTokenSet->get($refreshTokenSet->refresh_token->is($refresh_token));

        if (!$refreshToken) {
            return false;
        } else {
            $refreshToken->expires = strtotime($refreshToken->expires);
        }

        return $refreshToken->getValues();
    }

    /**
     * @param string $refresh_token
     * @param mixed  $client_id
     * @param mixed  $user_id
     * @param string $expires
     * @param string $scope
     * @return bool
     */
    public function setRefreshToken($refresh_token, $client_id, $user_id, $expires, $scope = null)
    {
        // convert expires to datestring
        $expires = date('Y-m-d H:i:s', $expires);

        $refreshTokenSet = new \LibOAuthServer_RefreshTokenSet();
        $refreshToken = $refreshTokenSet->get($refreshTokenSet->refresh_token->is($refresh_token));

        if (!$refreshToken) {
            $refreshToken = $refreshTokenSet->newRecord();
            $refreshToken->refresh_token = $refresh_token;
            $refreshToken->setPrimaryKeyModified(true);//BUG WITH string primarykey
        }

        $refreshToken->client_id = $client_id;
        $refreshToken->expires = $expires;
        $refreshToken->scope = $scope;
        $refreshToken->user_id = $user_id;

        return $refreshToken->save();
    }

    /**
     * @param string $refresh_token
     * @return bool
     */
    public function unsetRefreshToken($refresh_token)
    {
        $refreshTokenSet = new \LibOAuthServer_RefreshTokenSet();

        return $refreshTokenSet->delete($refreshTokenSet->refresh_token->is($refresh_token));
    }

    /**
     * plaintext passwords are bad!  Override this for your application
     *
     * @param array $user
     * @param string $password
     * @return bool
     */
    protected function checkPassword($user, $password)
    {
        require_once $GLOBALS['babInstallPath'] . '/utilit/password.class.php';

        if (! \bab_Password::verify($password, $user['password'], $user['password_hash_function'])) {
            return false;
        }

        return true;
    }

    // use a secure hashing algorithm when storing passwords. Override this for your application
    protected function hashPassword($password)
    {
        require_once $GLOBALS['babInstallPath'] . '/utilit/password.class.php';

        return \bab_Password::hash($password);
    }

    /**
     * @param string $username
     * @return array|bool
     */
    public function getUser($username)
    {
        $userSet = new \LibOAuthServer_UserSet();
        $user = $userSet->getFromUsername($username);

        if (!$user) {
            return false;
        }

        // the default behavior is to use "username" as the user_id
        $user['user_id'] = $username;

        return $user;
    }

    /**
     * CAN NOT CREATE USER from this end!
     *
     * @param string $username
     * @param string $password
     * @param string $firstName
     * @param string $lastName
     * @return bool
     */
    public function setUser($username, $password, $firstName = null, $lastName = null)
    {
        throw new \ErrorException('You can not create user from this end');
        return false;
    }

    /**
     * Add scopes to database
     * @param string $scope     Scopes space separated
     * @param boolean $default  If null the value will not but change if already exist, if it does not exist it will be false
     */
    public function addScope(string $scope, $default = null)
    {
        $scopeSet = new \LibOAuthServer_ScopeSet();

        $scopes = explode(' ', trim($scope));
        foreach ($scopes as $scope) {
            $scopeRecord = $scopeSet->get($scopeSet->scope->is($scope));

            if (!$scopeRecord) {
                $scopeRecord = $scopeSet->newRecord();
                $scopeRecord->scope = $scope;
                $scopeRecord->default = false;
                $scopeRecord->setPrimaryKeyModified(true);//BUG WITH string primarykey
            }
            if ($default !== null) {
                $scopeRecord->default = $default;
            }
            $scopeRecord->save();
        }

        return true;
    }

    /**
    * Remove scope(s) to database
    * @param string $scope     Scopes space separated
    */
    public function deleteScope(string $scope)
    {
        $scopeSet = new \LibOAuthServer_ScopeSet();

        $scopes = explode(' ', trim($scope));
        foreach ($scopes as $scope) {
            $scopeSet->delete($scopeSet->scope->is($scope));
        }

        return true;
    }


    /**
     * All scope have to exist
     * @param string $scope
     * @return bool
     */
    public function scopeExists($scope)
    {
        $scope = explode(' ', $scope);

        $scopeSet = new \LibOAuthServer_ScopeSet();
        $scopes = $scopeSet->select($scopeSet->scope->in($scope));

        if ($scopes && $scopes->count()) {
            return $scopes->count() == count($scope);
        }

        return false;
    }

    /**
     * if no scope return null
     * if scope but no default scope availabe retrun false
     * else return scopes space separated
     * @param mixed $client_id
     * @return null|string|false
     */
    public function getDefaultScope($client_id = null)
    {
        $scopeSet = new \LibOAuthServer_ScopeSet();
        $scopes = $scopeSet->select();

        if (!$scopes) {
            return null;
        }

        $scopeSet = new \LibOAuthServer_ScopeSet();
        $scopes = $scopeSet->select($scopeSet->is_default->is(true));

        if (!$scopes) {
            return false;//NO DEFAULT SCOPE BUT SCOPE ARE USED
        }

        $defaultScopes = array();
        foreach($scopes as $scope) {
            $defaultScopes[] = $scope->scope;
        }


        if ($client_id !== null) {
            $clientSet = new \LibOAuthServer_ClientSet();
            $client = $clientSet->get($clientSet->client_id->is($client_id));

            if ($client && $client->scope) {
                $clientScops = explode(' ', trim($client->scope));
                foreach ($defaultScopes as $scopIndex => $scopeValue) {
                    if (!in_array($scopeValue, $clientScops)) {
                        unset($defaultScopes[$scopIndex]);
                    }
                }
            }
        }

        if (count($defaultScopes) === 0) {
            return false;//NO DEFAULT SCOPE FOR THIS USER BUT SCOPE ARE USED
        }

        return implode(' ', $defaultScopes);
    }

    /**
     * @param mixed $client_id
     * @param $subject
     * @return string|false
     */
    public function getClientKey($client_id, $subject)
    {

        $jwtSet = new \LibOAuthServer_JwtSet();
        $jwt = $jwtSet->get(
            $jwtSet->client_id->is($client_id)
            ->_AND_($jwtSet->subject->is($subject))
        );

        if (!$jwt) {
            return false;
        }

        return $jwt->public_key;
    }

    /**
     * @param mixed $client_id
     * @return bool|null
     */
    public function getClientScope($client_id)
    {
        if (!$clientDetails = $this->getClientDetails($client_id)) {
            return false;
        }

        if (isset($clientDetails['scope'])) {
            return $clientDetails['scope'];
        }

        return null;
    }

    /**
     * @param mixed $client_id
     * @param $subject
     * @param $audience
     * @param $expires
     * @param $jti
     * @return array|null
     */
    public function getJti($client_id, $subject, $audience, $expires, $jti)
    {
        $jtiSet = new \LibOAuthServer_JtiSet();
        $jtiRecord = $jtiSet->get(
            $jtiSet->issuer->is($client_id)
            ->_AND_($jtiSet->subject->is($subject))
            ->_AND_($jtiSet->audience->is($audience))
            ->_AND_($jtiSet->expires->is($expires))
            ->_AND_($jtiSet->jti->is($jti))
        );

        if ($jtiRecord) {
            return $jtiRecord->getValues();
        }

        return null;
    }

    /**
     * @param mixed $client_id
     * @param $subject
     * @param $audience
     * @param $expires
     * @param $jti
     * @return bool
     */
    public function setJti($client_id, $subject, $audience, $expires, $jti)
    {
        $jtiSet = new \LibOAuthServer_JtiSet();

        $record = $jtiSet->newRecord();
        $record->issuer = $client_id;
        $record->subject = $subject;
        $record->audience = $audience;
        $record->expires = $expires;
        $record->jti = $jti;

        return $record->save();
    }

    /**
     * @param mixed $client_id
     * @return mixed
     */
    public function getPublicKey($client_id = null)
    {
        $publicKeySet = new \LibOAuthServer_PublicKeySet();

        $publicKey = $publicKeySet->get($publicKeySet->client_id->is($client_id));

        if (!$publicKey) {
            return false;
        }

        return $publicKey->public_key;
    }

    /**
     * @param mixed $client_id
     * @return mixed
     */
    public function getPrivateKey($client_id = null)
    {
        $publicKeySet = new \LibOAuthServer_PublicKeySet();

        $publicKey = $publicKeySet->get($publicKeySet->client_id->is($client_id));

        if (!$publicKey) {
            return false;
        }

        return $publicKey->private_key;
    }

    /**
     * @param mixed $client_id
     * @return string
     */
    public function getEncryptionAlgorithm($client_id = null)
    {
        $publicKeySet = new \LibOAuthServer_PublicKeySet();

        $publicKey = $publicKeySet->get($publicKeySet->client_id->is($client_id));

        if (!$publicKey) {
            return 'RS256';
        }

        return $publicKey->encryption_algorithm;


    }

    public function clearExpires()
    {
        $AccessTokenSet = new \LibOAuthServer_AccessTokenSet();
        $CodeSet = new \LibOAuthServer_CodeSet();
        $JtiSet = new \LibOAuthServer_JtiSet();
        $RefreshTokenSet = new \LibOAuthServer_RefreshTokenSet();


        $expires = date('Y-m-d H:i:s', time());

        $AccessTokenSet->delete($AccessTokenSet->expires->lessThan($expires));
        $CodeSet->delete($CodeSet->expires->lessThan($expires));
        $JtiSet->delete($JtiSet->expires->lessThan($expires));
        $RefreshTokenSet->delete($RefreshTokenSet->expires->lessThan($expires));

        return true;

    }
}