��          \      �       �      �      �      �   -        1  (   J     s  W  �     �       #     6   =  2   t  F   �  7   �                                       Error, path not found. Path saved. Python executable path Python not found or version is not version 3. Session run py file path The path to the session py is mandatory. The python path is mandatory. Project-Id-Version: LibAlgo
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2018-03-28 16:40+0200
PO-Revision-Date: 2018-03-28 16:42+0200
Last-Translator: Antoine GALLET <antoine.gallet@cantico.fr>
Language-Team: Cantico <support@cantico.fr>
Language: fr_FR
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Poedit-SourceCharset: iso-8859-1
X-Poedit-Basepath: ../programs
X-Poedit-KeywordsList: LibAlgo_translate;LibAlgo_translate:1,2
X-Generator: Poedit 2.0.1
X-Poedit-SearchPath-0: .
X-Poedit-SearchPathExcluded-0: api
 Erreur, chemin non trouvé. Chemin enregistré. Chemin vers l'exécutable Python V3 Exécutable Pyhon non trouvé ou version incompatible. Chemin vers le fichier run.py d'analyse de session Le chemin vers le fichier run.py d'analyse de session est obligatoire. Le chemin vers l'exécutable Python V3 est obligatoire. 